# Whatzitdo?

Appends the text of emoji after the icon, on sites that provide alternative text that is not another emoji.

Why? Well, ever wondered what the heck an emoji was on a github pull request? This extension adds the text after the image. Sure, that might not help you understand what poodle-monkey-headblanket actually means in context; but at least you've got a slightly better shot.

This is NOT intended as an accessibility extension, as it only reveals information already available in the DOM. In many cases it would simply repeat the announcement of the emoji.

## Install via Chrome store

Go to [Demoji in the Chrome store](https://chrome.google.com/webstore/detail/demoji/jabjlgenmoheljailheghhiifkpfllao) and add to Chrome.

## Development/testing

There is a test.html in the root that can do local testing. To test as an actual extension:

1. Clone repo
2. Open [chrome://extensions](chrome://extensions) URL
3. Make sure "Developer mode" is checked
4. Click "Load unpacked extension..."
5. Navigate to /chrome/ folder in your clone and select it

## Icons

16x16	Favicon on the extension's pages and context menu icon.
32x32	Windows computers often require this size.
48x48	Displays on the extension management page.
128x128	Displays on installation and in the Chrome Web Store.

https://developer.chrome.com/docs/extensions/mv3/user_interface/

## Store images

JPEG or 24-bit PNG (no alpha)

* Screenshots: 1280x800 or 640x400, 1min-5max
* Small promo tile: 440x280
* Marquee promo tile: 1400x560

## Release process

1. update version in manifest
2. zip the extension directory
3. upload new zip to chrome store via https://chrome.google.com/webstore/developer/dashboard
4. commit & tag that version `git tag -a VERSION -m 'version VERSION' && git push origin --tags`

## Why are you documenting a process only you can follow?

Indiana Jones: Can't you remember?
Henry Jones: I wrote them down in my diary, so that I wouldn't have to remember them.
