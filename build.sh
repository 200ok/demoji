#!/usr/bin/env bash
set -e

EMOJI_JSON="./node_modules/emojilib/dist/emoji-en-US.json"
DEMOJI_DIR="./demoji"

if [ -f $EMOJI_JSON ]
then
    echo "Copying emoji JSON source"
    cp $EMOJI_JSON $DEMOJI_DIR
else
    npm install
    cp $EMOJI_JSON $DEMOJI_DIR
fi

if [ "$(command -v zip)" ]; then 

    if [ -f "$DEMOJI_DIR.zip" ]; then
      echo "Deleting $DEMOJI_DIR.zip"
      rm -f "$DEMOJI_DIR.zip"
    fi

  echo "Creating new $DEMOJI_DIR.zip"
  zip -r demoji $DEMOJI_DIR
else 
  echo "ERROR zip NOT available - install it and try again";
fi

echo "Demoji built."