const url = chrome.runtime.getURL('emoji-en-US.json');

// Doing the dance to get JSON loaded
fetch(url).then((resp) => resp.json()).then(function (jsonData) {
    const $body = document.getElementsByTagName('body')[0];
    const emojiMap = jsonData;
    let idcount = 0;

    // deboucne for de mutation observer
    debounce = function (func, wait, immediate) {
        var result;
        var timeout = null;
        return function () {
            var context = this, args = arguments;
            var later = function () {
                timeout = null;
                if (!immediate) result = func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) result = func.apply(context, args);
            return result;
        };
    };

    /**
     * No real point replacing an emoji with...an emoji
     * Noting the spec for this is .... interesting.
     * Numbers and some punctuation detect as emoji.
     */
    function isEmoji(str) {
        return /\p{Emoji}/u.test(str)
    }

    /**
     * Identifies button by role, tagname or type (input)
     */
    function isButton(el) {
        const tag = el.tagName || '';
        const role = el.getAttribute('role') || '';
        const type = el.getAttribute('type') || '';
        return (tag.toLowerCase() === 'button' || role.toLowerCase() === 'button' || type.toLowerCase() === 'button')
    }

    // used to add in the aria-desc without deleting anything already there
    function appendToAttr(el, attr, val) {
        if (el.getAttribute(attr)) {
            el.setAttribute(attr, `${el.getAttribute(attr)} ${val}`)
        } else {
            el.setAttribute(attr, val)
        }
    }

    /**
     * Since many sites use alt text :emoji_name:, we strip out 
     * the colons and replace underscores with spaces.
     * ":emoji_name:" becomes "emoji name"
     */
    function cleanAltText(str) {
        let newStr = str || "";
        newStr = newStr.replace(/\_/g, ' ');
        newStr = newStr.replace(/\:/g, '');
        return newStr
    }

    /**
     * A lot of the work goes on here. Steps through a set of 
     * attributes looking for text and emoji values. If only 
     * emoji found, will try to look up the name of that emoji
     * from the loaded JSON map.
     */
    function getAltText(el) {

        let alt = false; // default return

        // where to look for alternative text, in order of preference
        let attrs = ['alt', 'title', 'alias'] 

        // used to split up potential values according to type
        let text = [] 
        let emoji = [] 
        
        // ye olde loop so we can push out of it
        for (var i = 0; i < attrs.length; i++) {
            let x = el.getAttribute(attrs[i]) || false;
            if(x) {
                if(isEmoji(x)) {
                    emoji.push(x.toString())
                } else {
                    text.push(x.toString())
                }
            }
        }
        
        if (text.length) {
            alt = text[0] // if we have text strings, use the first one
        } else if(emoji.length) {
            let x = emoji[0] // if we have emoji strings, use the first one
            alt = (emojiMap[x]) ? emojiMap[x][0] : x; // try to map from emoji to name
        }

        return cleanAltText(alt) // strip : and _
    }
    
    /**
     * DOM manipulation - the actual demojificiation
     */
    function demojify(el, str) {
        const $parent = el.parentElement;
        const demojiID = `chromeextensiondemoji${idcount}`;

        if ($parent) {

            // Exclude emoji in buttons - usually controls that shouldn't be touched
            if (!isButton($parent)) {

                $parent.style.width = "auto";

                let $text = document.createElement('span');
                    $text.setAttribute('id', demojiID)
                    $text.classList.add('chromeextension-demoji');
                    $text.append(`(${str})`);

                el.after($text)
                appendToAttr(el, 'aria-describedby', demojiID)
            } else {
                // if the button has no title, add the emoji name
                if ($parent.getAttribute('title') === null) {
                    $parent.setAttribute('title', str)
                }
            }
        }
    }

    function demoji() {

        /**
         * FINDING TARGETS
         * Any class that contains 'emoji' as a substring
         * Case-insensitive backups - belt and braces, browser support for these seems a little shaky
         * g-emoji is github
         * gl-emoji is gitlab
         * ...no, there is not an efficient way to query custom element names by substring ;)
         */
        const $emoji = document.querySelectorAll('img[class*="emoji"]:not(.demoji-skip), img[class="emoji" i]:not(.demoji-skip), img[class*="emoji" i]:not(.demoji-skip), img[src*="emoji"]:not(.demoji-skip), g-emoji:not(.demoji-skip), gl-emoji:not(.demoji-skip)');
        let demojied = 0;
        let notDemojied = 0;

        $emoji.forEach((el) => {
            let emojiText = getAltText(el);
            if (emojiText) {
                demojify(el, emojiText)
                el.classList.add('demoji-skip')
                demojied++;
            } else {
                el.classList.add('demoji-skip')
                notDemojied++
            }
        })

        // Since repeat runs may not find new emoji, only log if they do.
        if (demojied > 0 || notDemojied > 0) {
            console.log(`Demoji extension: ${demojied} images demojified, ${notDemojied} could not be demojied.`);
        }
    }

    function mutationCallback() {
        demoji()
    }

    setTimeout(function () {
        const observer = new MutationObserver(debounce(mutationCallback, 1000));
        // assuming here that new emoji images will be new child elements
        observer.observe($body, { attributes: false, childList: true, subtree: false });
    }, 1000);

    // chrome seems to fire this about... 50% of the time?!
    window.addEventListener('load', function () {
        demoji()
    });

    setTimeout(function () {
        demoji()
    }, 1000);

});
